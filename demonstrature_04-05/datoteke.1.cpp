#include <iostream>
#include <fstream>
#include <string>

/* Zadatak s demonstratura */

/**
 * Želimo napraviti polje po podacima iz datoteka.txt,
 * uspisati polje u datoteku,
 * ispisati sadržaj datoteke na ekran.
 */

using namespace std;

// Dodana funkcija za spremanje vrijednosti iz datoteke.txt u polje[]
void unosIzDatoteke(int polje[]);
// Ispis polja na ekran
void ispisPolje(int polje[]);
// Unos podataka iz polja u datoteku
void unosDatoteka(int polje[]);
// Čitanje podataka iz datoteke
void ispisIzDatoteke();

int main()
{
    int polje[12];
    // Unos polja čitanjem podataka iz datoteke (neće raditi ako datoteka ne postoji)
    unosIzDatoteke(polje);

    cout << "Ispis polja" << endl;
    ispisPolje(polje);

    // Unos polja u datoteku (raditi će ako datoteka ne postoji)
    unosDatoteka(polje);

    cout << "Citanje sadrzaja iz datoteka.txt" << endl;
    ispisIzDatoteke();

    return 0;
}


void unosIzDatoteke(int polje[])
{
    /*  Dohvaćanje podataka iz datoteka.txt  */
    ifstream ispis;
    ispis.open("datoteka.txt");

    if (ispis.fail())
    {
        cout << "Datoteka nije uspjesno otvorena." << endl;
        // Tu ne možemo prekinuti izvođenje programa jer funkcija
        // ne može tako lako prekinuti izvođenje funkcije main()
    }

    // Brojač kojeg povećavamo za svaku liniju datoteke
    int i = 0;
    // Svaka linija datoteke biti će string kojeg želimo spremiti
    string linija_datoteke;
    // Kada koristimo while, ne trebamo znati veličinu polja (broj linija)
    while (getline(ispis, linija_datoteke))
    {
        // Izmjena vrijednosti u polje[]
        polje[i] = stoi(linija_datoteke); // stoi("12345") = (int) 12345
        i++;
    }

    ispis.close();

    cout << "Unos vrijednost polja iz datoteke je uspjesan." << endl;
}


void unosDatoteka(int polje[])
{
    /**
     * Dohvaćanje podataka iz output.txt
     * (to će biti prazan string "" ako output.txt ne postoji) 
    */
    ofstream unos;
    unos.open("output.txt");

    if (unos.fail())
    {
        cout << "Datoteka nije uspjesno otvorena." << endl;
    }

    /*  Unos u datoteku  */
    for (int i = 0; i < 12; i++)
    {
        unos << polje[i] << endl;
    }

    cout << "Unos u datoteku je uspjesan" << endl;
    unos.close();
}

void ispisIzDatoteke()
{
    /*  Dohvaćanje podataka iz datoteka.txt  */
    ifstream ispis;
    ispis.open("datoteka.txt");

    if (ispis.fail())
    {
        cout << "Datoteka nije uspjesno otvorena." << endl;
    }

    /*  Ispis podataka iz datoteka.txt  */
    string linija_datoteke;
    while (getline(ispis, linija_datoteke))
    {
        cout << linija_datoteke << endl;
    }

    ispis.close();
}

void ispisPolje(int polje[])
{
    for (int i = 0; i < 12; i++)
    {
        cout << polje[i] << endl;
    }
}
