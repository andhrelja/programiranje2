#include <iostream>
#include <fstream>
#include <ctime>
#include <string>

/* Zadatak s demonstratura */

/**
 * Automatski generiramo polje,
 * uspisati polje u datoteku,
 * ispisati sadržaj datoteke na ekran.
 */

using namespace std;

const int velicina_polja = 12;
const string datoteka = "datoteka.txt";

void ispisPolje(int polje[]);
// Unos polja u datoteku
void unosDatoteka(int polje[]);
// Čitanje podataka iz datoteke
void ispisIzDatoteke();

int main()
{
    int polje[velicina_polja];

    // Funkcija rand() izračunava broj na temelju početnog,
    // zadanog broja (računalo ga određuje)
    srand(time(0)); 
    // srand() postavlja početni broj na time(0),
    // pa je zadani broj svakog izvođenja drugačiji

    for (int i = 0; i < velicina_polja; i++)
        polje[i] = 1 + rand() % 100;

    unosDatoteka(polje);

    cout << "Ispis polja" << endl;
    ispisPolje(polje);

    cout << "Citanje sadrzaja iz datoteka.txt" << endl;
    ispisIzDatoteke();

    return 0;
}

void unosDatoteka(int polje[])
{
    /*  Unos u datoteku  */
    ofstream unos;
    unos.open(datoteka);

    // Tu ne možemo prekinuti izvođenje programa jer funkcija
    // ne može tako lako prekinuti izvođenje funkcije main()
    if (unos.fail())
        cout << "Datoteka nije uspjesno otvorena." << endl;

    for (int i = 0; i < 12; i++)
        unos << polje[i] << endl;

    cout << "Unos polja u datoteku je uspjesan" << endl;
    unos.close();
}

void ispisIzDatoteke()
{
    /*  Dohvaćanje podataka iz datoteka.txt  */
    ifstream ispis;
    ispis.open(datoteka);

    if (ispis.fail())
        cout << "Datoteka nije uspjesno otvorena." << endl;

    /*  Ispis iz datoteka.txt  */
    string linija_datoteke;
    while (getline(ispis, linija_datoteke))
        cout << linija_datoteke << endl;

    ispis.close();
}

void ispisPolje(int polje[])
{
    for (int i = 0; i < 12; i++)
        cout << polje[i] << endl;
}
