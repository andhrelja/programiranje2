#include <iostream>
#include <fstream>
#include <string>

/* Zadatak s demonstratura */

/**
 * Želimo ispisati polje na ekran,
 * uspisati polje u datoteku,
 * ispisati sadržaj datoteke na ekran.
 */

using namespace std;

void ispisPolje(int polje[]);

int main()
{
    int polje[] = {1260, 1180, 1020, 1020, 980, 960, 1000, 1130, 1060, 1240, 1200, 1310};

    cout << "Ispis polja" << endl;
    ispisPolje(polje);

    /*  Unos u datoteku  */
    ofstream unos;
    unos.open("datoteka.txt");
    if (unos.fail())
    {
        cout << "Datoteka nije uspjesno otvorena." << endl;
    }
    for (int i = 0; i < 12; i++)
    {
        unos << polje[i] << endl;
    }

    cout << "Unos u datoteku je uspjesan" << endl;
    unos.close();

    /*  Ispis iz datoteke.txt  */
    ifstream ispis;
    ispis.open("datoteka.txt");

    if (ispis.fail())
    {
        cout << "Datoteka nije uspjesno otvorena." << endl;
        // Ako želimo prekinuti izvođenje programa, želimo return ili exit()
        // return 0;
        exit(0);
    }

    cout << "Čitanje sadržaja iz datoteka.txt" << endl;
    
    string linija_datoteke;
    while (getline(ispis, linija_datoteke))
    {
        cout << linija_datoteke << endl;
    }

    ispis.close();
}


void ispisPolje(int polje[])
{
    for (int i = 0; i < 12; i++)
    {
        cout << polje[i] << endl;
    }
}
