# Demonstrature 05.04.2019.

## Unos u datoteku, sortiranja, pretraživanja

### vjezba.cpp
Napisati program koji će zadano polje zapisati u datoteku.
To isto polje zatim sortirati (Selection sort) i ispisati.
Isto tako, napisati funkciju za pretraživanje koja će vratiti index traženog elementa.

### vjezba1.cpp
Napisati program koji će pri pozivu konobaru ispisati poruku "Konobar će sada izvršiti vašu narudžbu". Pića su unaprijed definirana u polju menu[].
Korisniku ponuditi ispis pića iz menija te pitati koje piće želi naručiti.
Zatim korisniku ispisati iznos koji mora platiti. Iznos za piće je definiran ovisno o odabranom piću.

### vjezba2.cpp
- Primjer za čitanje općina i njihovih poštanskih brojeva iz datoteke
U ovome zadatku želimo isčitati podatke iz datoteke i spremiti ih u polja s gradovima i polja s postanskim brojevima. Implementirati ćemo i funkciju za pretraživanje gradova po nazivu i poštanskom broju.
Obzirom da se u istoj liniji datoteke nalaze naziv grada i poštanski broj grada, elementi parova vrijednosti će imati istu poziciju u oba polja.

### datoteke.1.cpp
### datoteke.2.cpp
### datoteke.cpp
-- Zadaci s demonstratura --