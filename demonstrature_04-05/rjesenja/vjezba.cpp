#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

void selectionSort(int polje[], int n);
void ispisPolja(int polje[], int n);
void zamjena(int &a, int &b);

int pretraga(int polje[], int n, int x);

int main()
{
    int polje[] = {1260, 1180, 1020, 1020, 980, 960, 1000, 1130, 1060, 1240, 1200, 1310};

    /*  Unos u datoteku  */
    ofstream unos;
    unos.open("datoteka.txt");

    for (int i = 0; i < 12; i++)
    {

        polje[i] = 900 + rand() % 601;
        unos << polje[i] << endl;
    }

    unos.close();

    /*  Ispis iz datoteke  */
    string broj;
    ifstream ispis;
    ispis.open("datoteka.txt");

    while (getline(ispis, broj))
    {
        cout << broj << endl;
    }

    cout << "----------------" << endl;
    ispis.close();

    /*  Sortiranje polja  */
    selectionSort(polje, 12);
    //  - ispis sortiranog polja
    ispisPolja(polje, 12);

    /*   Pretraga   */
    int trazi;
    cout << "Unesite broj koji želite pretražiti:\t";
    cin >> trazi;

    int pozicija;
    pozicija = pretraga(polje, 12, trazi);
    if (pozicija != -1)
    {
        cout << "Element se nalazi na " << pozicija << ". poziciji." << endl;
    }

    return 0;
}

void selectionSort(int polje[], int n)
{
    int min_index;

    for (int i = 0; i < (n - 1); i++)
    {
        min_index = i;
        for (int j = (i + 1); j < n; j++)
        {
            if (polje[j] < polje[min_index])
            {
                min_index = j;
            }
        }
        zamjena(polje[i], polje[min_index]);
    }
}

void ispisPolja(int polje[], int n)
{
    for (int i = 0; i < n; i++)
    {
        cout << polje[i] << endl;
    }
}

int pretraga(int polje[], int n, int x)
{
    for (int i = 0; i < n; i++)
    {
        if (polje[i] == x)
        {
            return i;
        }
    }
    return -1;
}

void zamjena(int &a, int &b)
{
    int temp = a;
    a = b;
    b = temp;
}