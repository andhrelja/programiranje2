#include <iostream>
#include <string>

using namespace std;

void zoviKonobara();

string naruciPice(string menu[]);
int donesiRacun(string pice);

int main()
{
    zoviKonobara();

    string menu[] = {"voda", "sok", "cola"};
    string pice;
    pice = naruciPice(menu);

    int iznos;
    iznos = donesiRacun(pice);

    cout << "Platili ste " << iznos << "kn." << endl;

    return 0;
}

void zoviKonobara()
{
    cout << "Konobar će sada zaprimiti vašu narudžbu." << endl;
}

string naruciPice(string menu[])
{
    cout << "Kako vas mogu poslužiti?" << endl;
    cout << "Danas na meniju nudimo:" << endl;

    for (int i = 0; i < 3; i++)
    {
        cout << "-" << menu[i] << endl;
    }
    cout << endl;

    string pice;
    cout << "Unos: ";
    cin >> pice;

    return pice;
}

int donesiRacun(string pice)
{
    if (pice == "voda")
    {
        return 10;
    }
    else if (pice == "sok")
    {
        return 12;
    }
    else if (pice == "cola")
    {
        return 14;
    }
    else
    {
        return 0;
    }
}