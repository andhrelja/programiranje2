#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

#define N 849 // Broj linija u datoteci (veličina polja)

void ucitajGradove(string gradovi[], int post_brojevi[]);
void ispisPodataka(string gradovi[], int post_brojevi[]);
void insertionSort(int polje[]);
void ispisPostBrojevi(int post_brojevi[]);
void ispisGradova(string gradovi[]);

void pretragaPostBroj(int post_brojevi[], int trazi);
void pretragaGrad(string gradovi[], string trazi);

int main()
{
    string gradovi[N];
    int postanski_brojevi[N];

    ucitajGradove(gradovi, postanski_brojevi);
    ispisPodataka(gradovi, postanski_brojevi);

    insertionSort(postanski_brojevi);
    // ispisPostBroj(postanski_brojevi);
    // ispisGradova(gradovi);

    char odg;
    string grad;
    int post_broj;

    cout << "Pretraga: " << endl;
    cout << "Upisite g za pretragu gradova ili p za pretragu poštanskih brojeva" << endl;
    cin >> odg;

    if(odg == 'g')
    {
        cout << "Trazi grad: ";
        cin >> grad;
        pretragaGrad(gradovi, grad);
    }
    else if(odg == 'p')
    {
        cout << "Trazi postanski broj: ";
        cin >> post_broj;
        pretragaPostBroj(postanski_brojevi, post_broj);
    }
    else 
    {
        exit(1);
    }

    return 0;
}

void ucitajGradove(string gradovi[], int post_brojevi[])
{
    ifstream ispis;
    ispis.open("../gradovi.txt");

    if (ispis.fail())
        cout << "Datoteka nije učitana." << endl;

    string linija;
    int j = 0;
    while (getline(ispis, linija))
    {
        for (int i = 0; i < linija.length(); i++)
        {
            if (linija[i] == ' ')
            {
                string grad[] = {linija.substr(0, i)};
                gradovi[j] = grad[0];

                string temp = linija.substr(i + 1, linija.length() - 1);
                int postanski_broj[] = {atoi(temp.c_str())};
                post_brojevi[j] = postanski_broj[0];
                break;
            }
        }
        j++;
    }
}

void ispisPodataka(string gradovi[], int post_brojevi[])
{
    cout << "** Ispis podataka o gradovima s pripadnim postanskim brojevima **" << endl;
    cout << "---------------------------------------" << endl;
    for (int i = 0; i < N; i++)
    {
        cout << "Grad: " << gradovi[i] << endl;
        cout << "Postanski broj: " << post_brojevi[i] << endl;
        cout << "---------------------------------------" << endl;
    }
}

void insertionSort(int polje[])
{
    int min_element, j;

    for (int i = 0; i < N; i++)
    {
        min_element = polje[i];
        j = i;
        while (j > 0 && polje[j - 1] > min_element)
        {
            polje[j] = polje[j - 1];
            j--;
        }
        polje[j] = min_element;
    }
}

void ispisPostBrojevi(int post_brojevi[])
{
    for (int i = 0; i < N; i++)
    {
        cout << post_brojevi[i] << endl;
    }
}

void ispisGradova(string gradovi[])
{
    for (int i = 0; i < N; i++)
    {
        cout << gradovi[i] << endl;
    }
}

void pretragaPostBroj(int post_brojevi[], int trazi)
{
    bool pronadjen = false;

    for (int i = 0; i < N; i++)
    {
        if(trazi == post_brojevi[i])
        {
            cout << "Broj " << trazi << " se nalazi na " << i << ". poziciji." << endl;
            pronadjen = true;
        }
    }

    if(pronadjen == false)
        cout << "Broj " << trazi << " ne postoji u polju." << endl;
}

void pretragaGrad(string gradovi[], string trazi)
{
    bool pronadjen = false;

    for (int i = 0; i < N; i++)
    {
        if(trazi == gradovi[i])
        {
            cout << "Broj " << trazi << " se nalazi na " << i << ". poziciji." << endl;
            pronadjen = true;
        }
    }

    if(pronadjen == false)
        cout << "Broj " << trazi << " ne postoji u polju." << endl;
}