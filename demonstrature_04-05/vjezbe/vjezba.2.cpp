#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

#define N 849 // Broj linija u datoteci (veličina polja)

void ucitajGradove(string gradovi[], int post_brojevi[]);
void insertionSort(int polje[]);
void ispisPostBrojevi(int post_brojevi[]);
void ispisGradova(string gradovi[]);

void pretragaPostBroj(int post_brojevi[], int trazi);
void pretragaGrad(string gradovi[], string trazi);

int main()
{
    /**
     * Spremiti gradove i poštanske brojeve iz datoteke gradovi.txt
     * u polja gradovi i postanski_brojevi.
     * Ispisati sve gradove.
    */
    string gradovi[N];
    int postanski_brojevi[N];

    // ucitajGradove();
    // ispisGradova();

    /**
     * Sortirati polje postanski_brojevi i
     * ispisati sve postanske brojeve.
    */

    // insertionSort();
    // ispisPostBrojevi();

    /**
     * Napraviti upit i pretražiti unijetu vrijednost.
    */

    // pretragaGrad();
    // pretragaPostBroj();

    return 0;
}

void ucitajGradove(string gradovi[], int post_brojevi[])
{
    // Otvoriti datoteku i provjeriti je li otvorena
    // Hint: getline()

    string linija;
    int j = 0;
    // Prolazimo kroz svaku liniju
    while (getline(ispis, linija))
    {
        // Prolazimo kroz svako slovo u liniji
        for (int i = 0; i < linija.length(); i++)
        {
            // Prolazimo kroz svako slovo u liniji i tražimo razmak
            if (linija[i] == ' ')
            {
                // Kada pronađemo razmak, lijevu stranu dodajemo u polje gradovi,
                // a desnu stranu u polje postanski_brojevi
                // Svaka linija je oblika: Zagreb 10000

                string grad[] = {linija.substr(0, i)};
                gradovi[j] = grad[0];

                // Kako iz datoteke isčitavamo string,
                // moramo ga pretvoriti u int

                string temp = linija.substr(i + 1, linija.length() - 1);
                int postanski_broj[] = {atoi(temp.c_str())}; // atoi("123") = (int) 123
                post_brojevi[j] = postanski_broj[0];
                break;
            }
        }
        j++;
    }
}

void insertionSort(int polje[])
{

}

void ispisPostBrojevi(int post_brojevi[])
{

}

void ispisGradova(string gradovi[])
{

}

void pretragaPostBroj(int post_brojevi[], int trazi)
{
    bool pronadjen = false;
}

void pretragaGrad(string gradovi[], string trazi)
{
    bool pronadjen = false;
}