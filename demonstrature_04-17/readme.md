# Demonstrature 17.04.2019.

## Spremanje podataka iz datoteka, rekurzija

### vjezba.1.cpp
Napisati program koji će iz datoteke izvući ocjenu i naziv kolegija (naziv kolegija ne smije sadržavati razmake).
Zatim, te podatke je potrebno spremiti u polje s ocjenama i polje s imenima kolegija.
Za kraj ispisati sumu i produkt ocjena te prosjek.

### vjezba.2.cpp
Napisati program koji će upitati korisnika za unos podataka u polje proizvoljne veličine.
Zatim ispisati polje rekurzivnom funkcijom, a drugom rekurzivnom funkcijom izračunati sumu i produkt elemenata polja.