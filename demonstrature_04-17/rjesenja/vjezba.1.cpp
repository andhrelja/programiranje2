#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    int ocjene[7], produkt = 1;
    float suma = 0;
    string str[7];

    ifstream izlaz;
    izlaz.open("datoteka.txt");

    for (int i = 0; i < 7; i++)
    {
        izlaz >> ocjene[i] >> str[i];
    }

    for (int i = 0; i < 7; i++)
    {
        cout << ocjene[i] << " - " << str[i] << endl;
        suma += ocjene[i];
        produkt *= ocjene[i];
    }

    cout << "Suma ocjena: " << suma << endl;
    cout << "Produkt ocjena: " << produkt << endl;
    cout << "Prosjek: " << suma / 7 << endl;

    izlaz.close();

    return 0;
}