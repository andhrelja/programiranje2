#include <iostream>

using namespace std;

void ispisRek(int polje[], int n)
{
    if (n < 0)
    {
        return;
    }
    cout << n + 1 << ". element: ";
    cout << polje[n] << endl;
    ispisRek(polje, n - 1);
}

void suma_produkt(int polje[], int n, int &suma, int &produkt)
{
    if (n < 0)
    {
        return;
    }
    suma += polje[n];
    produkt *= polje[n];
    suma_produkt(polje, n - 1, suma, produkt);
}

int main()
{
    int n;
    cout << "Unesite broj elemenata polja: ";
    cin >> n;

    int polje[n];
    for (int i = 0; i < n; i++)
    {
        cout << i + 1 << ". element: ";
        cin >> polje[i];
        cout << endl;
    }

    cout << "Običan ispis" << endl;

    for (int i = 0; i < n; i++)
    {
        cout << i + 1 << ". element: ";
        cout << polje[i] << endl;
    }

    int i = n - 1;
    cout << "Rekurzivan ispis" << endl;
    ispisRek(polje, i);

    // Suma i produkt
    int suma = 0;
    int produkt = 1;
    for (int i = 0; i < n; i++)
    {
        suma += polje[i];
        produkt *= polje[i];
    }

    cout << "Suma: " << suma << endl;
    cout << "Produkt: " << produkt << endl;

    // Suma i produkt - rekurzivno
    int sum = 0;
    int pro = 1;
    int j = n - 1;

    suma_produkt(polje, j, sum, pro);

    cout << "Suma (rekurzivno): " << sum << endl;
    cout << "Produkt (rekurzivno): " << pro << endl;

    return 0;
}