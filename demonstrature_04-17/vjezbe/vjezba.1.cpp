#include <iostream>
#include <fstream>

using namespace std;
/**
 * Napisati program koji će iz datoteke isčitati ocjene,
 * te spremiti ih u polje. Zatim proći kroz polje te stvoriti
 * sumu i produkt svih ocjena. Kada imamo sumu, želimo
 * izračunati prosjek. Zatim prikazati sumu, produkt i prosjek.
 * # Hint: za korištenje decimalnih brojeva, treba nam float
 */

int main()
{
    int ocjene[7];
    int produkt = 1;
    float suma = 0;

    ifstream izlaz;
    izlaz.open("datoteka.txt");

    // Unos brojeva u polje

    // Prolazak kroz polje i staranje sume i produkta

    // Ispis sume, produkta i prosjeka

    izlaz.close();

    return 0;
}