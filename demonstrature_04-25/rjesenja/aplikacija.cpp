#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void ispisRekuzija(int polje[], int brojac);
void ispisObican(int polje[], int brojac);
void insertionSort(int arr[], int n);

int main()
{
    cout << "------------ Aplikacija ------------" << endl;
    cout << "1 - Ispisi polje (obican ispis)" << endl;
    cout << "2 - Ispisi polje (rekurzija)" << endl;
    cout << "3 - Sortiraj polje" << endl;

    int unos;
    cout << "Unos: ";
    cin >> unos;

    int polje[10];

    srand(time(NULL));
    for(int i=0; i < 10; i++)
    {
        polje[i] = rand() % 100;
    }

    if(unos == 1)
    {
        ispisObican(polje, 10);
    }
    else if(unos == 2)
    {
        cout << "--- Rekurzivan ispis polja ---" << endl;
        ispisRekuzija(polje, 9);
    }
    else if(unos == 3)
    {
        insertionSort(polje, 10);
        cout << "--- Rekurzivan ispis sortiranog polja ---" << endl;
        ispisRekuzija(polje, 9);
    }



    return 0;
}

void ispisObican(int polje[], int brojac)
{
    cout << "--- Obican ispis polja ---" << endl;
    for(int i = 0; i < brojac; i++)
    {
        cout << i + 1 << ". pozicija:" << polje[i] << endl;
    }
}
void ispisRekuzija(int polje[], int brojac)
{
    if (brojac < 0)
    {
        return;
    }
    cout << brojac << ":" << polje[brojac] << endl;
    ispisRekuzija(polje, brojac - 1);
}

void insertionSort(int arr[], int n)
{
    int i, key, j;
    for (i = 1; i < n; i++) {
        key = arr[i];
        j = i - 1;

        /* Move elements of arr[0..i-1], that are
          greater than key, to one position ahead
          of their current position */
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}
