# Demonstrature 16.05.2019.

## Objektno orjentirano programiranje

### Company.cpp
Napraviti program koji sadrži zapise o Tvrtki (naziv tvrtke) i njezinim zaposlenicima.
Zaposleni mogu biti CEO (Chief executive officer) ili CFO (Chief financial officer). Program sadrži zapise o imenu i prezimenu zaposlenika. Želimo ispisati ime i prezime zaposlenika putem jedne funkcije (metode).

Program može i ne mora imati zadane vrijednosti (poput naziva tvrtke, imena i prezimena zaposlenika).

### Lists.cpp
Napraviti povezanu listu i funkciju koja će vratiti veličinu liste.