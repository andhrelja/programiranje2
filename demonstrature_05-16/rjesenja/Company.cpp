#include <iostream>

using namespace std;

class Employee
{
public:
    int id;
    string first_name;
    string last_name;

    Employee(string f_name, string l_name)
    {
        this->first_name = f_name;
        this->last_name = l_name;
    }

    void printFullName()
    {
        cout << this->first_name << " " << this->last_name << endl;
    }

    ~Employee() {}
};

class Company
{
public:
    int id;
    string name;

    Employee *ceo;
    Employee *cfo;

    Company(int id, string name)
    {
        this->id = id;
        this->name = name;

        this->ceo = new Employee("John", "Jones");
        this->cfo = new Employee("Dave", "Smith");
    }
    ~Company() {}
};

int main()
{
    string company_name = "Huawei Ltd";
    Company company = Company(1, company_name);

    cout << company.name << endl;
    company.ceo->printFullName();
    company.cfo->printFullName();

    string company_name_1 = "Apple Ltd";
    Company company_1 = Company(2, company_name_1);

    cout << company_1.name << endl;
    company_1.ceo->printFullName();
    company_1.cfo->printFullName();

    return 0;
}