#include <iostream>

using namespace std;


class Node{
public:
    int data;
    Node *next;
    // Konstruktorom postavljamo zadanu vrijednost
    // pri stvaranju objekta u funkciji main()
    Node(int d)
    {
        this->data = d;
    }

    // Destruktorom dealociramo memoriju
    ~Node() {}
};


int listLength(Node *head)
{
    int cnt = 1;
    // Moramo proći kroz svaki node->next dok god
    // ne dođemo do pokazivača na 0 (NULL pokazivač),
    // počevši od početnog (head).
    // U ovom slučaju, to je head->next->next->next->next->NULL
    while(head->next != NULL)
    {
        head = head->next;
        cnt++;
    }
    return cnt;
}


int main()
{
    // Kako stvaramo pokazivače na klasu Node,
    // dohvaćamo njihove atribute pomoću node->data umjesto node.data.
    Node *head = new Node(5);
    Node *nodeA = new Node(4);
    Node *nodeB = new Node(3);
    Node *nodeC = new Node(2);
    Node *nodeD = new Node(1);

    head->next = nodeA;
    nodeA->next = nodeB;
    nodeB->next = nodeC;
    nodeC->next = nodeD;

    int lenght = listLength(head);
    cout << "Duljina povezane liste: " << lenght << endl;

    return 0;
}
