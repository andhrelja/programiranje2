# Demonstrature 23.05.2019.

## Povezane liste, pokazivači na polja

### Liste.cpp

Napraviti strukturu Čvor koja će sadržavati podatak i pokazivač na sljedeći element povezane liste.  
Zatim definirati i napisati funkcije za:  
-- unos elementa na kraju liste  
-- unos elementa na početku liste  
-- brisanje elementa po danoj vrijednosti  
-- ispis povezane liste  


### polja.cpp

Napraviti program koji će se sastojati od funkcije za stvaranje polja (povratna vrijednost je pokazivač na polje), te funkcije za ispis stvorenog polja. Polje se može sastojati od random ili vrijednosti unijete od korisnika.

### 2d_polja.cpp

Napraviti program koji će se sastojati od funkcije za stvaranje 2d polja (povratna vrijednost je pokazivač na pokazivač na polje), te funkcije za ispis stvorenog polja. Polje se može sastojati od random ili vrijednosti unijete od korisnika.


### Vježbe 9

-- zadatak1.cpp --  
-- zadatak2.cpp --  
-- zadatak3.cpp --  