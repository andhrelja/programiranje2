#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int **stvoriPolje(int n, int m);
void ispis(int **polje, int n, int m);

int main()
{
    int n = 5;
    int m = 4;
    int **polje;
    srand(time(0));
    
    polje = stvoriPolje(n, m);
    ispis(polje, n, m);

    delete[] polje;

    return 0;
}

int **stvoriPolje(int n, int m)
{
    int **polje = new int*[n];

    for (int i = 0; i < n; i++)
    {
        polje[i] = new int[m];
        for (int j = 0; j < m; j++)
        {
            polje[i][j] = rand() % 100 + 1;
        }
    }

    return polje;
}

void ispis(int **polje, int n, int m)
{
    for (int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            cout << polje[i][j] << "\t";
        }
        cout << endl;  
    }
}
