#include <iostream>

using namespace std;

struct Node {
    int data;
    Node *next;
};

void insert(Node *head, int data);
Node *prepend(Node *head, int data);
void deleteValue(Node *head, int value);
void print(Node *head);

int main()
{
    Node *head = new Node;

    insert(head, 5);
    insert(head, 4);

    head = prepend(head, 3);
    head = prepend(head, 2);

    deleteValue(head, 5);
    
    print(head);

    return 0;
}

void insert(Node *head, int data)
{
    Node *node = new Node;

    if(head->next == NULL){
        node->data = data;
        head->next = node;
    }
    else
    {
        node = head;
        while (node->next != NULL)
        {
            node = node->next;
        }
        Node *newNode = new Node;
        newNode->data = data;
        node->next = newNode;
    }
}

Node *prepend(Node *head, int data)
{
    Node *newHead = new Node;

    newHead->next = head;
    head->data = data;

    return newHead;
}

void deleteValue(Node *head, int value)
{
    if(head->next == NULL){
        return;
    }
    else
    {
        Node *node = head;
        while (node->next != NULL)
        {
            if(node->next->data == value){
                node->next = node->next->next;
                return;
            }
            node = node->next;
        }
    }
}

void print(Node *head)
{
    Node *node = head;
    int count = 0;

    while (node->next != NULL)
    {
        count++;
        node = node->next;
        cout << count << ". element: " << node->data << endl;
    }
}