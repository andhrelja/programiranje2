#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int *stvoriPolje(int n);
void ispis(int *polje, int n);

int main()
{
    int n = 5;
    int *polje;
    srand(time(0));
    
    polje = stvoriPolje(n);
    ispis(polje, n);

    delete[] polje;

    return 0;
}

int *stvoriPolje(int n)
{
    int *polje = new int[n];

    for (int i = 0; i < n; i++)
    {
        polje[i] = rand() % 100 + 1;
    }

    return polje;
}

void ispis(int *polje, int n)
{
    for (int i = 0; i < n; i++)
    {
        cout << polje[i] << "\n"; 
    }
}
