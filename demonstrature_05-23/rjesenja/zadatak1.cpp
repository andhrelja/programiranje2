#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{

    int n;
    cout << "Unesite velicinu polja" << endl;
    cin >> n;

    srand(time(0));
    int *polje = new int[n];
    
    for (int i = 0; i < n; i++)
    {
        *(polje + i) = rand() % 100 + 1;
    }

    for (int i = 0; i < n; i++)
    {
        cout << "Element na poziciji " << i << ": " << *(polje + i) << endl;
    }

    delete[] polje;

    return 0;
}