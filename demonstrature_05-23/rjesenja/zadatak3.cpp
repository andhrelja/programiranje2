#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int **stvoriMatricu(int n, int m);
void kvadriraj(int **polje, int n, int m);
void ispis(int **polje, int n, int m);

int main()
{
    int n = 5;
    int m = 4;
    int **matrica;
    srand(time(0));
    
    matrica = stvoriMatricu(n, m);
    kvadriraj(matrica, n, m);
    ispis(matrica, n, m);

    // Koristimo for petlju za dealokaciju memorije
    // zato što svaki element matrice sadrži pokazivač na polje
    for(int i = 0; i < n; i++)
    {
        delete[] matrica[i];
    }
    delete[] matrica;

    return 0;
}

int **stvoriMatricu(int n, int m)
{
    int **polje = new int*[n];

    for (int i = 0; i < n; i++)
    {
        *(polje+i) = new int[m];
        for (int j = 0; j < m; j++)
        {
            *(*(polje+i)+j) = rand() % 100 + 1;
        }
    }

    return polje;
}

void kvadriraj(int **polje, int n, int m)
{
    for (int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            polje[i][j] *= polje[i][j];
            // *(*(polje+i)+j) *= *(*(polje+i)+j)
        } 
    }
}

void ispis(int **polje, int n, int m)
{
    for (int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            cout << *(*(polje+i)+j) << "\t";
        }
        cout << endl;  
    }
}
