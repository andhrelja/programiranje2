#include <iostream>

using namespace std;

void insert(Node *head, int data);
Node *prepend(Node *head, int data);
void deleteValue(Node *head, int value);
void print(Node *head);

int main()
{
    Node *head = new Node;

    insert(head, 5);
    insert(head, 4);

    head = prepend(head, 3);
    head = prepend(head, 2);

    deleteValue(head, 5);
    
    print(head);

    return 0;
}

void insert(Node *head, int data)
{

}

Node *prepend(Node *head, int data)
{

}

void deleteValue(Node *head, int value)
{

}

void print(Node *head)
{

}