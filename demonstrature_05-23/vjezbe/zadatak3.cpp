#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int **stvoriMatricu(int n, int m);
void kvadriraj(int **polje, int n, int m);
void ispis(int **polje, int n, int m);

int main()
{
    int n = 5;
    int m = 4;
    int **matrica;
    srand(time(0));
    
    matrica = stvoriMatricu(n, m);
    kvadriraj(matrica, n, m);
    ispis(matrica, n, m);

    // Koristimo for petlju za dealokaciju memorije
    // zato što svaki element matrice sadrži pokazivač na polje

    return 0;
}

int **stvoriMatricu(int n, int m)
{
    int **polje = new int*[n];


    return polje;
}

void kvadriraj(int **polje, int n, int m)
{

}

void ispis(int **polje, int n, int m)
{

}
