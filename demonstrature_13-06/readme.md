# Demonstrature 13.06.2019.

## Stog i red

### red.cpp

Napisati program koji omogućuje push i pop nad redom te ispisuje red.

### stog.cpp

Napisati program koji omogućuje push i pop nad stogom te ispisuje stog.

### Vježba za završni ispit

Nadopuniti red i stog tako da se korisniku ponudi glavni izbornik. U glavnom izborniku program nudi opcije za unos elementa u (red ili stog), dohvaćanje elemenata (reda ili stoga) ili izlaz iz programa.