#include <iostream>
#include <iomanip> // setw

using namespace std;

struct Node{
    int data;
    Node *next;
};

int pop(Node *&head, Node *&tail);
void push(Node *&head, Node *&tail, int data);
void print(Node *head);

int main()
{
    Node *head = NULL, *tail = NULL;
    push(head, tail, 1);
    push(head, tail, 2);
    push(head, tail, 3);

    print(head);
    cout << "1. pop(): " << pop(head, tail) << endl;
    cout << "2. pop(): " << pop(head, tail) << endl;
    cout << "3. pop(): " << pop(head, tail) << endl;

    print(head);

    return 0;
}

int pop(Node *&head, Node *&tail)
{
    if(head == tail)
    {
        tail = NULL;
    }

    int data = head->data;
    head = head->next;

    return data;
}

void push(Node *&head, Node *&tail, int data)
{
    Node *tmp = new Node;
    tmp->data = data;
    tmp->next = NULL;

    if(!head)
        head = tmp;
    else
        tail->next = tmp;

    tail = tmp;
}

void print(Node *head)
{
    while(head)
    {
        cout << head->data << setw(4);
        head = head->next;
    }
    cout << endl;
}