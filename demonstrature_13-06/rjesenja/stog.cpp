#include <iostream>
#include <iomanip> // setw

using namespace std;

struct Node{
    int data;
    Node *next;
};

int pop(Node *&head);
void push(Node *&head, int data);
void print(Node *head);

int main()
{
    Node *head = NULL;
    push(head, 1);
    push(head, 2);
    push(head, 3);

    print(head);
    cout << "1. pop(): " << pop(head) << endl;
    cout << "2. pop(): " << pop(head) << endl;

    print(head);

    return 0;
}

int pop(Node *&head)
{
    int data = head->data;
    head = head->next;
    
    return data;
}

void push(Node *&head, int data)
{
    Node *tmp = new Node;
    tmp->data = data;
    tmp->next = head;
    head = tmp;
}

void print(Node *head)
{
    while(head)
    {
        cout << head->data << setw(4);
        head = head->next;
    }
    cout << endl;
}