#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void ispisPolja(int polje[], int n);
void ispisDatoteka(string datoteka, int n);
void ispisMatrice(int polje[], int n, string output, int trazeni_element);

void selectionSort(int polje[], int n, string tip);

bool pretrazivanje(int polje[], int n, int x);
int pozicija(int polje[], int n, int x);

int main()
{
    int n = 50;
    int polje[50];

    string datoteka = "brojevi.txt";
    string output = "output.txt";
    string broj;

    ifstream unos;
    unos.open(datoteka);

    int i = 0;
    while (getline(unos, broj))
    {
        polje[i] = stoi(broj);
        i++;
    }

    ispisDatoteka(datoteka, n);

    cout << "----------- Stvoreno polje --------------" << "\n";
    ispisPolja(polje, n);

    cout << "----------- Selection sort -------------- (silazno)" << "\n";
    selectionSort(polje, n, "silazno");
    ispisPolja(polje, n);

    cout << "----------- Selection sort -------------- (uzlazno)" << "\n";
    selectionSort(polje, n, "uzlazno");
    ispisPolja(polje, n);

    int unos_pretrage;
    cout << "Unesite element koji zelite pretraziti ili 0 za prekid:\t";
    cin >> unos_pretrage;

    if (pretrazivanje(polje, n, unos_pretrage))
    {
        cout << "Traženi element postoji." << endl;
        cout << "Traženi element se nalazi na poziciji " << pozicija(polje, n, unos_pretrage) << "." << endl;
    }
    else
    {
        cout << "Traženi element ne postoji." << endl;
    }

    ispisMatrice(polje, n, output, unos_pretrage);
}

void ispisDatoteka(string datoteka, int n)
{
    ofstream ispis;
    ispis.open(datoteka);

    for (int i = 0; i < n; i++)
    {
        ispis << rand() % 500 + 1 << endl;
    }

    ispis.close();

    cout << "Ispis u datoteku je uspjesan." << "\n";
}

void ispisPolja(int polje[], int n)
{
    for (int i = 0; i < n; i++)
    {
        cout << polje[i] << endl;
    }
}

bool pretrazivanje(int polje[], int n, int x)
{
    for (int i = 0; i < n; i++)
    {
        if (polje[i] == x)
        {
            return true;
        }
    }
    return false;
}

int pozicija(int polje[], int n, int x)
{
    for (int i = 0; i < n; i++)
    {
        if (polje[i] == x)
        {
            return i;
        }
    }
    return -1;
}

void selectionSort(int polje[], int n, string tip)
{
    int min_index, max_index, temp;

    if (tip == "silazno")
    {
        for (int i = 0; i < (n - 1); i++)
        {
            min_index = i;

            int j = i + 1;
            while (j < n)
            {
                if (polje[j] < polje[min_index])
                {
                    min_index = j;
                }
                j++;
            }
            temp = polje[i];
            polje[i] = polje[min_index];
            polje[min_index] = temp;
        }
    }
    else if (tip == "uzlazno")
    {
        for (int i = 0; i < (n - 1); i++)
        {
            max_index = i;
            for (int j = (i + 1); j < n; j++)
            {
                if (polje[j] > polje[max_index])
                {
                    max_index = j;
                }
            }
            temp = polje[i];
            polje[i] = polje[max_index];
            polje[max_index] = temp;
        }
    }
}

void ispisMatrice(int polje[], int n, string output, int trazeni_element)
{
    int k = 0;
    ofstream ispis;
    ispis.open(output);

    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            ispis << polje[k] << " ";
            k++;
        }
        ispis << "\n";
    }

    ispis << "\nTrazeni element: " << trazeni_element << "\n";
    ispis << "\nPozicija trazenog elementa: " << pozicija(polje, n, trazeni_element) << "\n";
    ispis.close();

    cout << "Ispis u datoteku je uspjesan." << endl;
}