#include <iostream>
#include <fstream>
#include <string>

using namespace std;

const int n = 50;

int *stvoriPolje(string datoteka);

void ispisPolja(int polje[]);
void ispisDatoteka(string datoteka);
void ispisMatrice(int polje[], string datoteka, int trazeni_element);

void selectionSort(int polje[], string tip);
bool tipSortiranja(int a, int b, string tip);

int unosPretrage();
bool pretrazivanje(int polje[], int x, bool index = false);

int main()
{
    int *polje = new int[n];
    int unos_pretrage = 1;

    string datoteka = "brojevi.txt";
    string output = "output.txt";

    ispisDatoteka(datoteka);

    cout << "----------- Stvoreno polje --------------" << endl;
    polje = stvoriPolje(datoteka);
    ispisPolja(polje);

    cout << "----------- Selection sort -------------- (silazno)" << endl;
    selectionSort(polje, "silazno");
    ispisPolja(polje);

    cout << "----------- Selection sort -------------- (uzlazno)" << endl;
    selectionSort(polje, "uzlazno");
    ispisPolja(polje);

    unos_pretrage = unosPretrage();
    if (pretrazivanje(polje, unos_pretrage))
    {
        cout << "Traženi element postoji." << endl;
        pretrazivanje(polje, unos_pretrage, true);
    }
    else
    {
        cout << "Traženi element ne postoji." << endl;
    }

    ispisMatrice(polje, output, unos_pretrage);
}

void ispisDatoteka(string datoteka)
{
    ofstream ispis;
    ispis.open(datoteka);

    for (int i = 0; i < n; i++)
    {
        ispis << rand() % 500 + 1 << endl;
    }

    ispis.close();

    cout << "Ispis u datoteku je uspjesan." << endl;
}

int *stvoriPolje(string datoteka)
{
    int *polje = new int[n];
    int i = 0;
    string broj;

    ifstream unos;
    unos.open(datoteka);

    while (getline(unos, broj))
    {
        polje[i] = stoi(broj);
        i++;
    }

    return polje;
}

void ispisPolja(int polje[])
{
    for (int i = 0; i < n; i++)
    {
        if (i < 10)
        {
            cout << polje[i] << endl;
        }
    }
    cout << "..." << endl;
}

void selectionSort(int polje[], string tip)
{
    int min_index, temp;

    for (int i = 0; i < (n - 1); i++)
    {
        min_index = i;
        for (int j = (i + 1); j < n; j++)
        {
            if (tipSortiranja(polje[j], polje[min_index], tip))
            {
                min_index = j;
            }
        }
        temp = polje[i];
        polje[i] = polje[min_index];
        polje[min_index] = temp;
    }
}

bool tipSortiranja(int a, int b, string tip)
{
    if (tip == "silazno")
    {
        return a < b;
    }
    else
    {
        return a > b;
    }
}

int unosPretrage()
{
    int x;
    cout << "Unesite element koji zelite pretraziti ili 0 za prekid:\t";
    cin >> x;

    return x;
}

bool pretrazivanje(int polje[], int x, bool index)
{
    for (int i = 0; i < n; i++)
    {
        if (polje[i] == x)
        {
            if (index == true)
            {
                cout << "Traženi element se nalazi na " << i+1 << ". poziciji" << endl;
            }
            return true;
        }
    }
    return false;
}

void ispisMatrice(int polje[], string datoteka, int trazeni_element)
{
    int k = 0;
    ofstream ispis;
    ispis.open(datoteka);

    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            ispis << polje[k] << " ";
            k++;
        }
        ispis << "\n";
    }

    ispis << "\nTrazeni element: " << trazeni_element << endl;
    // ispis << "\nPozicija trazenog elementa: " << pretrazivanje(polje, trazeni_element, true) << endl;
    ispis.close();

    cout << "Ispis u datoteku je uspjesan." << endl;
}