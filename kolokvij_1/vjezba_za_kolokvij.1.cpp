#include <iostream>
#include <fstream>

/* Ponavljanje za kolokvij */

/**
 * Otvoriti datoteku kreiranu u vjezba_za_kolokvij.cpp.
 * Učitati podatke iz datoteke u polje.
 * Sortirati polje na obratan način i ispisati ga
 * (ako je polje sortirano silazno, sortirati ga uzlazno).
 * Napraviti pretragu tako da korisnik upisuje broj koji želi pretražiti.
 * Pretraga ispisuje potvrdu i indeks broja ako je on pronađen,
 * te ispisuje poruku ako broj nije pronađen. 
 */

using namespace std;

void ispisPolja(int polje[]);
void sort(int polje[]);
void pretraga(int polje[], int trazi);

int main()
{
    int n;
    int polje[n];

    /*  Unijeti podatke iz datoteke  */
    ifstream ispis;
    ispis.open("datoteka.txt");
    // while(ispis): polje[i] = linija; i = [0, ..., n]

    /*  Ispisati polje  */
    // ispis(polje)

    /*  Sortirati polje  */
    // sort(polje)

    /*  Ispisati polje  */
    // ispis(polje)

    /*  Pretraga  */
    // pretraga(polje, trazi)

}


void ispisPolja(int polje[])
{

}
void sort(int polje[])
{
    
}
void pretraga(int polje[], int trazi)
{

}