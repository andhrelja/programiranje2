# Priprema za završni ispit  

## Povezane liste, red i stog  
  
Rješavanjem sljedećih zadataka usvojit ćete znanja potrebna za uspješan prolazak završnog ispita. Ugodno rješavanje! :)
  
### Liste.cpp  

Nadograditi dani kod tako da se osobe unose obzirom na godinu rodjenja (sortiranje moze biti uzlazno ili silazno). Isto tako, nadograditi main() funkciju na način da se korisniku ponudi izbor:
   
- unos novih osoba  
- brisanje postojecih osoba  
- ispis svih osoba  
- kraj programa  
  
### Red.cpp

Napisati program koji simulira rad u banci. Podaci o klijentu koje želimo spremiti su oib, ime, prezime, imaRacun (bool vrijednost koja nam kaže ima li klijent otvoreni račun u banci). Korisniku se treba ponuditi:
  
- dodavanje novih klijenata u red  
- primanje klijenata na šalter  
- broj klijenata koji čekaju u redu  
- ispis svih klijenata koji čekaju u redu  
- kraj programa  
  
### Stog.cpp  

Napisati program koji simulira narudžbe u restoranu. Podaci o narudžbi koje želimo spremiti su idNarudzbe, nazivObroka, cijenaObroka, pdv, ukupno (cijenaObroka * pdv). Korisniku se treba ponuditi:
  
- dodavanje nove narudžbe  
- izbacivanje trenutne narudžbe  
- ispis svih narudžbi  
- broj narudžbi na čekanju  
- kraj programa  
  

Na sve zadatke implementirati dealociranje memorije.