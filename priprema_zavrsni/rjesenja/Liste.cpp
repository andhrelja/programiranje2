#include <iostream>

using namespace std;

struct Osoba{
    int oib;
    string ime;
    string prezime;
    int godina_rodjenja;
    Osoba *next;
};

void unos(Osoba *osobe, int oib, string ime, string prezime, int godina_rodjenja);
void ispis(Osoba *osobe);
void izbrisi(Osoba *osobe, int oib);
void dealokacija(Osoba *&osobe);

int main()
{
    int odabir = -1;
    int oib, godina_rodjenja;
    string ime, prezime;

    Osoba *osobe = new Osoba;

    while(odabir != 0)
    {
        cout << "--- Glavni izbornik --- " << endl;
        cout << "1) Unos nove osobe" << endl;
        cout << "2) Ispis sviju osoba" << endl;
        cout << "3) Brisanje osobe" << endl;
        cout << "0) Kraj programa" << endl;
        cin >> odabir;

        switch (odabir)
        {
            case 1:
                cout << "\n-- Nova osoba --";
                cout << "\nOIB: ";
                cin >> oib;
                cout << "\nIme: ";
                cin >> ime;
                cout << "\nPrezime: ";
                cin >> prezime;
                cout << "\nGodina rodjenja: ";
                cin >> godina_rodjenja;

                unos(osobe, oib, ime, prezime, godina_rodjenja);
                break;

            case 2:
                ispis(osobe);
                break;
            case 3:
                cout << "Unesite OIB osobe koju zelite obrisati: ";
                cin >> oib;
                izbrisi(osobe, oib);
                cout << endl;
                break;
            default:
                break;
        }
    }

    dealokacija(osobe);

    return 0;
}

void unos(Osoba *osobe, int oib, string ime, string prezime, int godina_rodjenja)
{
    Osoba *osoba = new Osoba;
    osoba->oib = oib;
    osoba->ime = ime;
    osoba->prezime = prezime;
    osoba->godina_rodjenja = godina_rodjenja;

    while(osobe->next)
    {
        // tmp će uvijek biti jedan element ispred od onog trenutnog (trenutni je osobe)
        Osoba *tmp = osobe->next;
        if (tmp->godina_rodjenja > osoba->godina_rodjenja)
        {
            osoba->next = osobe->next;
            break;
        }
        osobe = osobe->next;
    }

    osobe->next = osoba;
}

void ispis(Osoba *osobe)
{
    cout << "--- Ispis sviju ljudi ---" << endl;
    while (osobe->next)
    {
        osobe = osobe->next;
        cout << "\nOIB: " << osobe->oib << endl;
        cout << "Ime: " << osobe->ime << endl;
        cout << "Prezime: " << osobe->prezime << endl;
        cout << "Godina rodjenja: " << osobe->godina_rodjenja << endl;
    }
}

void izbrisi(Osoba *osobe, int oib)
{
    while(osobe->next)
    {
        // tmp će uvijek biti jedan element ispred od onog trenutnog (trenutni je osobe)
        Osoba *tmp = osobe->next;
        if(tmp->oib == oib)
        {
            if (tmp->next) // Ovom provjerom pazimo da zadnji element povezane liste nije NULL
                osobe->next = tmp->next;
            else // Kada je, želimo da tako i ostane
                osobe->next = NULL;

            cout << "Osoba " << tmp->ime << " " << tmp->prezime << " je uspjesno izbrisana." << endl;

            // Brišemo tmp, ali zapravo ne brišemo element povezane liste, 
            // već samo postavljamo pokazivač na idući element u listi
            delete tmp;
            break;
        }
        osobe = osobe->next;
    }
}

void dealokacija(Osoba *&osobe)
{
    while(osobe->next)
    {
        Osoba *tmp = osobe->next;
        osobe = osobe->next;
        delete tmp;
    }
    osobe = NULL;
}