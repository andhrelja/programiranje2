#include <iostream>

using namespace std;

struct Cvor{
    int oib;
    string ime;
    string prezime;
    bool imaRacun;
    Cvor *next;
};

int pop(Cvor *&head, Cvor *&tail);
void push(Cvor *&head, Cvor *&tail, int oib, string ime, string prezime, bool imaRacun);
int brojCekanja(Cvor *&head);
void ispis(Cvor *head);
void dealokacija(Cvor *&head, Cvor *&tail);

int main()
{
    int odabir = -1;
    int oib;
    string ime, prezime;
    bool imaRacun;
    char racun;

    Cvor *head = NULL;
    Cvor *tail = NULL;

    while(odabir != 0)
    {
        cout << "--- Banka d.d. --- " << endl;
        cout << "1) Dodaj osobu u red" << endl;
        cout << "2) Ispis osoba u redu" << endl;
        cout << "3) Primanje na salter" << endl;
        cout << "4) Broj osoba u redu" << endl;
        cout << "0) Kraj programa" << endl;
        cin >> odabir;

        
        switch (odabir)
        {
            case 1:
                cout << "\n-- Nova osoba --";
                cout << "\nOIB: ";
                cin >> oib;
                cout << "\nIme: ";
                cin >> ime;
                cout << "\nPrezime: ";
                cin >> prezime;
                cout << "\nIma racun(d/n): ";
                cin >> racun;

                if(racun == 'd')
                    imaRacun = true;
                else
                    imaRacun = false;

                push(head, tail, oib, ime, prezime, imaRacun);
                break;

            case 2:
                ispis(head);
                break;
            case 3:
                cout << "OIB osobe koja ide na salter: " << pop(head, tail) << endl;
                break;
            case 4:
                cout << "Broj osoba u redu cekanja: " << brojCekanja(head) << endl;
                break;
        }
    }

    dealokacija(head, tail);

    return 0;
}

int pop(Cvor *&head, Cvor *&tail)
{
    int oib = head->oib;

    if (head == tail)
    {
        tail = NULL;
    }

    head = head->next;

    return oib;
}

void push(Cvor *&head, Cvor *&tail, int oib, string ime, string prezime, bool imaRacun)
{
    Cvor *tmp = new Cvor;
    tmp->oib = oib;
    tmp->ime = ime;
    tmp->prezime = prezime;
    tmp->imaRacun = imaRacun;
    tmp->next = NULL;

    if(head)
        tail->next = tmp;
    else
        head = tmp;

    tail = tmp;
}

void ispis(Cvor *head)
{
    cout << "-- Ispis osoba koje cekaju u redu --" << endl;
    while (head)
    {
        cout << "\nOIB: " << head->oib << endl;
        cout << "Ime: " << head->ime << endl;
        cout << "Prezime: " << head->prezime << endl;
        if(head->imaRacun)
            cout << "* ima racun" << endl;
        else
            cout << "* nema racun" << endl;

        head = head->next;
    }
    cout << endl;
}

int brojCekanja(Cvor *&head)
{
    int broj = 0;
    while (head)
    {
        head = head->next;
        broj++;
    }

    return broj;
}

void dealokacija(Cvor *&head, Cvor *&tail)
{
    while(head)
    {
        Cvor *tmp = head->next;
        head = head->next;
        delete tmp;
    }
    head = NULL;
    tail = NULL;
}