#include <iostream>

using namespace std;

struct Cvor{
    int idNarudzbe;
    string nazivObroka;
    float cijenaObroka;
    float pdv = 0.25;
    float cijenaUkupno;
    Cvor *next;
};

string pop(Cvor *&head);
void push(Cvor *&head, int id, string naziv, float cijena);
void ispis(Cvor *head);
int brojNarudzbi(Cvor *head);
void dealokacija(Cvor *&head);

int main()
{
    int odabir = -1;
    int idNarudzbe;
    string naziv;
    float cijena;

    Cvor *head = NULL;

    while(odabir != 0)
    {
        cout << "--- Pracenje narudzbi --- " << endl;
        cout << "1) Nova narudzba" << endl;
        cout << "2) Ispis svih narudzbi" << endl;
        cout << "3) Izbaci narudzbu" << endl;
        cout << "4) Broj narudzbi na cekanju" << endl;
        cout << "0) Kraj programa" << endl;
        cin >> odabir;

        
        switch (odabir)
        {
            case 1:
                cout << "\n-- Nova narudzba --";
                cout << "\nID narudzbe: ";
                cin >> idNarudzbe;
                cout << "\nNaziv obroka: ";
                cin >> naziv;
                cout << "\nCijena obroka: ";
                cin >> cijena;

                push(head, idNarudzbe, naziv, cijena);
                cout << endl;
                break;
            case 2:
                ispis(head);
                cout << endl;
                break;
            case 3:
                cout << "Izbacuje se obrok: " << pop(head) << endl;
                cout << endl;
                break;
            case 4:
                cout << "Broj narudzbi na cekanju: " << brojNarudzbi(head) << endl;
                cout << endl;
                break;
            default:
                break;
        }
    }

    dealokacija(head);

    return 0;
}

string pop(Cvor *&head)
{
    string naziv  = head->nazivObroka;
    head = head->next;
    
    return naziv;
}

void push(Cvor *&head, int id, string naziv, float cijena)
{
    Cvor *tmp = new Cvor;
    tmp->idNarudzbe = id;
    tmp->nazivObroka = naziv;
    tmp->cijenaObroka = cijena;

    float cijenaUkupno = tmp->cijenaObroka + tmp->cijenaObroka * tmp->pdv;
    tmp->cijenaUkupno = cijenaUkupno;
    tmp->next = head;
    head = tmp;
}

void ispis(Cvor *head)
{
    cout << "\n-- Ispis svih narudzbi --" << endl;
    while (head)
    {
        cout << endl;
        cout << "ID narudzbe: " << head->idNarudzbe << endl;
        cout << "Naziv obroka: " << head->nazivObroka << endl;
        cout << "Cijena obroka: " << head->cijenaObroka << endl;
        cout << "Cijena ukupno: " << head->cijenaUkupno << endl;
        head = head->next;
    }
    cout << endl;
}

int brojNarudzbi(Cvor *head)
{
    int broj = 0;
    while (head)
    {
        head = head->next;
        broj++;
    }
    return broj;
}

void dealokacija(Cvor *&head)
{
    while(head)
    {
        Cvor *tmp = head->next;
        head = head->next;
        delete tmp;
    }
    head = NULL;
}