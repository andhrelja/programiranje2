#include <iostream>

using namespace std;

struct Osoba{
    int oib;
    string ime;
    string prezime;
    int godina_rodjenja;
    Osoba *next;
};

void unos(Osoba *osobe, int oib, string ime, string prezime, int godina_rodjenja);
void ispis(Osoba *osobe);
void izbrisi(Osoba *osobe, int oib);

int main()
{
    Osoba *osobe = new Osoba;
    unos(osobe, 123, "Ivo", "Ivic", 1976);
    unos(osobe, 133, "Marko", "Markovic", 1987);
    unos(osobe, 143, "Mirko", "Mirkovic", 1999);
    ispis(osobe);

    izbrisi(osobe, 143);
    ispis(osobe);

    return 0;
}

void unos(Osoba *osobe, int oib, string ime, string prezime, int godina_rodjenja)
{
    Osoba *osoba = new Osoba;
    osoba->oib = oib;
    osoba->ime = ime;
    osoba->prezime = prezime;
    osoba->godina_rodjenja = godina_rodjenja;

    while(osobe->next)
    {
        osobe = osobe->next;
    }

    osobe->next = osoba;
}

void ispis(Osoba *osobe)
{
    cout << "--- Ispis sviju ljudi ---" << endl;
    while (osobe->next)
    {
        osobe = osobe->next;
        cout << "\nOIB: " << osobe->oib << endl;
        cout << "Ime: " << osobe->ime << endl;
        cout << "Prezime: " << osobe->prezime << endl;
        cout << "Godina rodjenja: " << osobe->godina_rodjenja << endl;
    }
}

void izbrisi(Osoba *osobe, int oib)
{
    while(osobe->next)
    {
        // tmp Ä‡e uvijek biti jedan element ispred od onog trenutnog (trenutni je osobe)
        Osoba *tmp = osobe->next;
        if(tmp->oib == oib)
        {
            if (tmp->next) // Ovom provjerom pazimo da zadnji element povezane liste nije NULL
                osobe->next = tmp->next;
            else // Kada je, Å¾elimo da tako i ostane
                osobe->next = NULL;

            // BriÅ¡emo tmp, ali zapravo ne briÅ¡emo element povezane liste, 
            // veÄ‡ samo postavljamo pokazivaÄ na iduÄ‡i element u listi
            delete tmp;
            break;
        }
        osobe = osobe->next;
    }
}