# Programiranje 2

  
Odjel za informatiku, Sveučilište u Rijeci

Nositelj kolegija: dr. sc. Ana Meštrović   
Asistent: Rebeka Lerga
  
# Teme:

  
1. Unos i ispis iz datoteka  
    - **demonstrature_04-05/**
    - **demonstrature_04-17/**
2. Sortiranja i pretraživanja   
    - **kolokvij_1/v5_ponavljanje/**
3. Funkcije i povratne vrijednosti
    - **demonstrature_04-05/**
4. Rekurzije  
    - **demonstrature_04-17/**
5. Objektno orjentirano programiranje  
    - **demonstrature_05-16/**